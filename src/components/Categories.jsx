import React, {useState} from 'react';

// class Categories extends React.Component {
//     state = {
//         activeItem: 0,
//     }
//
//     onSelectItem = index => {
//         this.setState({
//             activeItem: index,
//         });
//     }
//
//     render() {
//         const { items } = this.props;
//         return (
//             <div className="categories">
//                 <ul>
//                     <li className="active">Все</li>
//                     {items && items.map((name, index) =>
//                         <li
//                             className={this.state.activeItem === index ? 'active' : ''}
//                             onClick={() => this.onSelectItem(index)}
//                             key={`${name}_${index}`}>
//                             {name}
//                         </li>)}
//                 </ul>
//             </div>
//         );
//     }
// }



function Categories({ items }) {
    const [state, setState] = useState(null);

    const onSelectItem = index => {
        setState(index);
    }

    return (
        <div className="categories">
            <ul>
                <li
                    className={state === null ? 'active' : ''}
                    onClick={() => onSelectItem(null)}>
                    Все
                </li>
                {items && items.map((name, index) =>
                    <li
                        className={state === index ? 'active' : ''}
                        onClick={() => onSelectItem(index)}
                        key={`${name}_${index}`}>
                        {name}
                    </li>)}
            </ul>
        </div>
    );
}

export default Categories;